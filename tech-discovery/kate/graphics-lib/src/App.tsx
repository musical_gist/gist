import React from "react";
import "./App.css";
import entity from "./components/entity.json";

import { GStaff, IGStaffProps } from "./components/GStaff";
import { Entity } from "./entities/entities";

const App: React.FC = () => {
  const staffProps: IGStaffProps = {
    width: 400,
    entity: new Entity(entity)
  };
  console.log(staffProps.entity);
  return (
    <div style={{ padding: 50 }}>
      <GStaff {...staffProps} />
    </div>
  );
};

export default App;
