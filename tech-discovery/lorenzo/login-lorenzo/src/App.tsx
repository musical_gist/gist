import React from "react";
import GrommetExample from "./GrommetExample";

const App: React.FC = () => {
  return <GrommetExample />;
};

export default App;
