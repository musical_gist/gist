export class Note {
  constructor(data: any) {
    this.beat = data.beat;
    this.length = data.length;
  }
  beat: number;
  length: number;
}

export class Line {
  constructor(data: any) {
    this.notes = data.notes.map((note: any) => new Note(note));
  }
  notes: Note[];
}

export class Measure {
  constructor(data: any) {
    this.a = new Line(data.a);
    this.b = new Line(data.b);
    this.c = new Line(data.c);
    this.d = new Line(data.d);
    this.eLine = new Line(data.eLine);
    this.eSpace = new Line(data.eSpace);
    this.fLine = new Line(data.fLine);
    this.fSpace = new Line(data.fSpace);
    this.g = new Line(data.g);
  }
  a: Line;
  b: Line;
  c: Line;
  d: Line;
  eLine: Line;
  eSpace: Line;
  fLine: Line;
  fSpace: Line;
  g: Line;
}

export class Staff {
  constructor(data: any) {
    this.measures = data.measures.map((measure: any) => new Measure(measure));
  }
  measures: Measure[];
}

export class Entity {
  constructor(data: any) {
    this.staff = new Staff(data.staff);
  }
  staff: Staff;
}
