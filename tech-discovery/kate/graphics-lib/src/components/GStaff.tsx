import React, { Component } from "react";
import { GMeasure, IGMeasureProps } from "./GMeasure";
import { Entity, Measure } from "../entities/entities";
import entity from "./entity.json";

export interface IGStaffProps {
  width: number;
  entity: Entity;
}
interface IGStaffState {
  measures: Measure[];
}
export class GStaff extends Component<IGStaffProps, IGStaffState> {
  state = {
    measures: this.props.entity.staff.measures
  };

  addMeasure = () => {
    let { measures } = this.state;
    measures.push(new Measure(entity.staff.measures[0])); //for now just pushing json
    this.setState({
      measures: measures
    });
  };

  render() {
    let measureCount = this.state.measures.length;
    let measures = [];
    let count = 0;
    while (count < measureCount) {
      const measureProps: IGMeasureProps = {
        measure: this.props.entity.staff.measures[count],
        lineWidth: 150
      };

      const measure = (
        <div className="column">
          <GMeasure {...measureProps} />
        </div>
      );
      measures.push(measure);
      count++;
    }

    return (
      <>
        <div className="row" style={{ display: "flex", padding: 10 }}>
          {measures}
        </div>
        <div>
          <button onClick={this.addMeasure}>add measure</button>
        </div>
      </>
    );
  }
}
