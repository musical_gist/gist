import React from "react";
import Note from "./components/Note.component";

function App() {
  return (
    <div>
      <Note />
    </div>
  );
}

export default App;
