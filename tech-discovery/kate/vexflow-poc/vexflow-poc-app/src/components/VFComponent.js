import React, { Component } from "react";

import { getStaves, getNewRenderer, getNewContext } from "../helpers";

/**
 * props :
 *  - notes : VexNote []
 */
export class VFComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: this.props.notes,
      context: undefined
    };
  }

  /**
   * set vexflow boilerplate code
   */
  componentDidMount() {
    const { notes } = this.state;
    let renderer = getNewRenderer(this.refs.vf);
    let context = getNewContext(renderer);

    if (!(notes === undefined || notes.length === 0)) {
      getStaves(notes, context);
    }
    this.setState({
      context: context
    });
  }

  /**
   * when component updates, check if the notes being passed in
   * are different from current, then update if notes if need be
   * and redraw
   */
  componentDidUpdate = async prevProps => {
    if (this.props.notes !== prevProps.notes) {
      await this.setState({
        notes: this.props.notes
      });

      const { context, notes } = this.state;

      if (!(notes === undefined || notes.length === 0)) {
        getStaves(notes, context);
      }
    }
  };

  render() {
    return <div ref="vf" />;
  }
}
