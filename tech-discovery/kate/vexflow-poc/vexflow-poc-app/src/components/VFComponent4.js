import React, { Component } from "react";
import Vex from "vexflow";

/**
 * this component demonstrates how to add a note to a staff
 */
export default class VFComponent4 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: this.props.notes,
      stave: undefined,
      context: undefined,
      renderer: undefined
    };
  }

  /**
   * when component mounts, set up vexflow boilerplate code,
   * if any notes are passed in props, show them.
   * this has a lot of dirty this.state instead of setState
   * but i'll clean it up later
   */
  componentDidMount() {
    const VF = Vex.Flow;

    var div = this.refs.k8;
    this.state.renderer = new VF.Renderer(div, VF.Renderer.Backends.SVG);

    this.state.renderer.resize(500, 500);
    this.state.context = this.state.renderer.getContext();
    this.state.context.setFont("Arial", 10, "");

    this.state.stave = new VF.Stave(10, 40, 400);

    this.state.stave.addClef("treble").addTimeSignature("4/4");

    this.state.stave.setContext(this.state.context).draw();

    console.log(this.state.notes);

    if (!(this.state.notes === undefined || this.state.notes.length === 0)) {
      console.log("has notes");
      // Create a voice in 4/4 and add above notes
      var voice = new VF.Voice({ num_beats: 4, beat_value: 4 });
      voice.addTickables(this.state.notes);

      // Format and justify the notes to 400 pixels.
      var formatter = new VF.Formatter()
        .joinVoices([voice])
        .format([voice], 400);

      // Render voice
      voice.draw(this.state.context, this.state.stave);
    }
    console.log("no notes");
  }

  /**
   * when component updates, check if the notes being passed in
   * are different from current, then update if notes if need be
   * and redraw
   */
  componentDidUpdate = async prevProps => {
    if (this.props.notes !== prevProps.notes) {
      await this.setState({
        notes: this.props.notes
      });
      const VF = Vex.Flow;

      this.state.stave.setContext(this.state.context).draw();

      if (!(this.state.notes === undefined || this.state.notes.length === 0)) {
        // Create a voice in 4/4 and add above notes
        var voice = new Vex.Flow.Voice({ num_beats: 4, beat_value: 4 });
        voice.addTickables(this.state.notes);

        // Format and justify the notes to 400 pixels.
        var formatter = new VF.Formatter()
          .joinVoices([voice])
          .format([voice], 400);

        // Render voice
        voice.draw(this.state.context, this.state.stave);
      }
    }
  };

  render() {
    return (
      <>
        <div ref="k8" />
      </>
    );
  }
}
