import Vex from "vexflow";

/**
 * helpers.js :
 * helper functions for dealing with vexflow.
 * most of these functions have hardcoded parameters
 * at the moment that we eventually need to deal with
 * to (attempt to) make it dynamic.
 */

/**
 * helper function to create a new VexNote
 * @param {*} clef : string
 * @param {*} keys : []
 * @param {*} duration : string
 */
export function getVexNote(clef, keys, duration) {
  return new Vex.Flow.StaveNote({
    clef: clef,
    keys: keys,
    duration: duration
  });
}

/**
 * helper function to render measures of notes
 * @param {*} notes : notes to render, VexNote[]
 * @param {*} context : render context, Vex.Flow.Context
 *
 */
export function getStaves(notes, context) {
  for (let noteCount = 0; noteCount < notes.length; noteCount += 4) {
    let newStave;
    //set up first 'stave' (measure) to include clef/time information
    if (noteCount === 0) {
      newStave = new Vex.Flow.Stave(10, 10, 265); //200 + 65 to account for 65px of information
      newStave.addClef("treble").addTimeSignature("4/4"); //will need to make these based on parameters
    } else {
      newStave = new Vex.Flow.Stave(75 + noteCount * 50, 10, 200); //currently have it adding to width, need to make it add below (responsive problem)
    }
    newStave.setContext(context).draw();
    let voice = new Vex.Flow.Voice({
      num_beats: 4,
      beat_value: 4
    }); //need to make based on parameters
    voice.addTickables(notes.slice(noteCount, noteCount + 4)); //if not enough notes won't render rn
    new Vex.Flow.Formatter().joinVoices([voice]).format([voice], 200);
    voice.draw(context, newStave);
  }
}

/**
 * helper function to create new renderer
 * @param {*} div : div ref
 */
export function getNewRenderer(div) {
  let renderer = new Vex.Flow.Renderer(div, Vex.Flow.Renderer.Backends.SVG);
  renderer.resize(1000, 500);
  return renderer;
}

/**
 * helper function to create new context
 * @param {*} renderer : Vex.Flow.Renderer
 */
export function getNewContext(renderer) {
  let context = renderer.getContext();
  context.setFont("Arial", 20, "");
  return context;
}
