import React from "react";
//const TextInput = require("evergreen-ui");
import { Button as EvergreenButton, Spinner, IconButton } from "evergreen-ui";
import {
  Box,
  Button,
  Collapsible,
  Heading,
  Grommet,
  Layer,
  ResponsiveContext
} from "grommet";
import { FormClose, Notification } from "grommet-icons";

const theme = {
  global: {
    colors: {
      brand: "#228BE6"
    },
    font: {
      family: "Roboto",
      size: "14px",
      height: "20px"
    }
  }
};

const AppBar = (props: any) => (
  <Box
    tag="header"
    direction="row"
    align="center"
    justify="between"
    background="brand"
    pad={{ left: "medium", right: "small", vertical: "small" }}
    elevation="medium"
    style={{ zIndex: "1" }}
    {...props}
  />
);

export default class GrommetExample extends React.Component {
  onClick = () => {
    this.setState({
      showSidebar: !this.state.showSidebar
    });
  };

  evergreenButtonClick = () => {
    this.setState({
      showSidebar: !this.state.showSidebar
    });
  };

  back = () => {
    this.setState({
      showSidebar: !this.state.showSidebar
    });
  };

  state = {
    showSidebar: false
  };
  render() {
    const { showSidebar } = this.state;
    return (
      <Grommet theme={theme} full>
        <ResponsiveContext.Consumer>
          {size => (
            <Box fill>
              <AppBar>
                <Heading level="3" margin="none">
                  gist
                </Heading>
                <Button icon={<Notification />} onClick={this.onClick} />
              </AppBar>
              <Box direction="row" flex overflow={{ horizontal: "hidden" }}>
                <Box flex align="center" justify="center">
                  <EvergreenButton
                    marginRight={16}
                    appearance="minimal"
                    intent="success"
                    onClick={this.evergreenButtonClick}
                  >
                    gist it
                  </EvergreenButton>
                </Box>
                {!showSidebar || size !== "small" ? (
                  <Collapsible direction="horizontal" open={showSidebar}>
                    <Box
                      flex
                      width="medium"
                      background="light-2"
                      elevation="small"
                      align="center"
                      justify="center"
                    >
                      <Spinner />
                      <div style={{ padding: "30px" }}>
                        <IconButton
                          marginBottom={16}
                          appearance="minimal"
                          icon="cross"
                          onClick={this.back}
                        />
                      </div>
                    </Box>
                  </Collapsible>
                ) : (
                  <Layer>
                    <Box
                      background="light-2"
                      tag="header"
                      justify="end"
                      align="center"
                      direction="row"
                    >
                      <Button
                        icon={<FormClose />}
                        onClick={() => this.setState({ showSidebar: false })}
                      />
                    </Box>
                    <Box
                      fill
                      background="light-2"
                      align="center"
                      justify="center"
                    >
                      sidebar
                    </Box>
                  </Layer>
                )}
              </Box>
            </Box>
          )}
        </ResponsiveContext.Consumer>
      </Grommet>
    );
  }
}
