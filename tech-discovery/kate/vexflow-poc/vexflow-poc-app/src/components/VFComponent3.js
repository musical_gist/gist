import React, { Component } from "react";
import Vex from "vexflow";

/**
 * this component shows how to color a single note.
 * followed tutorial at https://github.com/0xfe/vexflow/wiki/Coloring-&-Styling-Notes
 */
export default class VFComponent3 extends Component {
  componentDidMount() {
    const VF = Vex.Flow;

    var div = this.refs.k8;
    var renderer = new VF.Renderer(div, VF.Renderer.Backends.SVG);

    renderer.resize(500, 500);
    var context = renderer.getContext();
    context.setFont("Arial", 10, "");

    var stave = new VF.Stave(10, 40, 400);

    stave.addClef("treble").addTimeSignature("4/4");

    var notes = [
      // A quarter-note C.

      new VF.StaveNote({ clef: "treble", keys: ["c/4"], duration: "q" }),

      // A quarter-note D.
      new VF.StaveNote({ clef: "treble", keys: ["d/4"], duration: "q" }),

      // A quarter-note rest. Note that the key (b/4) specifies the vertical
      // position of the rest.
      new VF.StaveNote({ clef: "treble", keys: ["b/4"], duration: "qr" }),

      // A C-Major chord.
      new VF.StaveNote({
        clef: "treble",
        keys: ["c/4", "e/4", "g/4"],
        duration: "q"
      })
    ];

    notes[0].setStyle({ fillStyle: "red", strokeStyle: "red" });
    // Create a voice in 4/4 and add above notes
    var voice = new VF.Voice({ num_beats: 4, beat_value: 4 });
    voice.addTickables(notes);

    // Format and justify the notes to 400 pixels.
    var formatter = new VF.Formatter().joinVoices([voice]).format([voice], 400);

    // Render voice
    voice.draw(context, stave);

    stave.setContext(context).draw();
  }
  render() {
    return <div ref="k8" />;
  }
}
