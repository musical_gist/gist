import React, { Component } from "react";
import { GLine, IGLineProps } from "./GLine";
import { Measure } from "../entities/entities";

export interface IGMeasureProps {
  measure: Measure;
  lineWidth: number;
  timeSignatureBeat?: number;
  timeSignatureNote?: number;
}

export class GMeasure extends Component<IGMeasureProps> {
  render() {
    const fLineProps: IGLineProps = {
      line: this.props.measure.fLine,
      width: 150,
      color: "black",
      topLine: true
    };
    const eSpaceProps: IGLineProps = {
      line: this.props.measure.eSpace,
      width: 150,
      color: "white"
    };
    const dLineProps: IGLineProps = {
      line: this.props.measure.d,
      width: 150,
      color: "black"
    };
    const cSpaceProps: IGLineProps = {
      line: this.props.measure.c,
      width: 150,
      color: "white"
    };
    const bLineProps: IGLineProps = {
      line: this.props.measure.b,
      width: 150,
      color: "black"
    };
    const aSpaceProps: IGLineProps = {
      line: this.props.measure.a,
      width: 150,
      color: "white"
    };
    const gLineProps: IGLineProps = {
      line: this.props.measure.g,
      width: 150,
      color: "black"
    };
    const fSpaceProps: IGLineProps = {
      line: this.props.measure.fSpace,
      width: 150,
      color: "white"
    };
    const eLineProps: IGLineProps = {
      line: this.props.measure.eLine,
      width: 150,
      color: "black"
    };
    return (
      <>
        <div
          style={{
            width: this.props.lineWidth,
            borderRight: "1px solid",
            paddingTop: "0px"
          }}
        >
          <GLine {...fLineProps} />
          <GLine {...eSpaceProps} />
          <GLine {...dLineProps} />
          <GLine {...cSpaceProps} />
          <GLine {...bLineProps} />
          <GLine {...aSpaceProps} />
          <GLine {...gLineProps} />
          <GLine {...fSpaceProps} />
          <GLine {...eLineProps} />
        </div>
      </>
    );
  }
}
