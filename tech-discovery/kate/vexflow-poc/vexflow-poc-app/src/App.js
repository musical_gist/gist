import React from "react";
import "./App.css";

import VFComponentWrapper from "./components/VFComponentWrapper";

function App() {
  return (
    <>
      {/* <VFComponent1 />
      <VFComponent2 />
      <VFComponent3 /> */}
      <VFComponentWrapper />
    </>
  );
}

export default App;
