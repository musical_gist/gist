import React from "react";
import MIDISounds from "../midi-sounds-react";

export default class Note extends React.Component {
  playSound = () => {
    /**
     * parameters
     *    instrument number, array of notes to be played, length of note(s)
     */
    this.midiSounds.playChordNow(3, [52, 55, 60], 5);
  };

  render() {
    return (
      <div>
        <button onClick={this.playSound}>Play!</button>
        <MIDISounds
          ref={ref => (this.midiSounds = ref)}
          appElementName="root"
          instruments={[3]}
        />
      </div>
    );
  }
}
