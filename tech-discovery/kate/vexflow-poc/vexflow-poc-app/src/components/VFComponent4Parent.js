import React, { Component } from "react";
import VFComponent4 from "./VFComponent4";
import Vex from "vexflow";

/**
 * this component wraps VFComponent4
 */
export default class VFComponent4Parent extends Component {
  state = {
    notes: []
  };
  addNotes = async () => {
    await this.setState({
      notes: [
        // A quarter-note C.
        new Vex.Flow.StaveNote({
          clef: "treble",
          keys: ["c/4"],
          duration: "q"
        }),

        // A quarter-note D.
        new Vex.Flow.StaveNote({
          clef: "treble",
          keys: ["d/4"],
          duration: "q"
        }),

        // A quarter-note rest. Note that the key (b/4) specifies the vertical
        // position of the rest.
        new Vex.Flow.StaveNote({
          clef: "treble",
          keys: ["b/4"],
          duration: "qr"
        }),

        // A C-Major chord.
        new Vex.Flow.StaveNote({
          clef: "treble",
          keys: ["c/4", "e/4", "g/4"],
          duration: "q"
        })
      ]
    });
    console.log(this.state.notes);
  };
  render() {
    return (
      <>
        <VFComponent4 notes={this.state.notes} />
        <button onClick={this.addNotes}>Add Notes</button>
      </>
    );
  }
}
