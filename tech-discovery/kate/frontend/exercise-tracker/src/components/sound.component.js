import React, { Component } from "react";
const MIDISounds = require("midi-sounds-react");

export default class Sound extends Component {
  constructor(props) {
    super(props);
  }

  playTestInstrument() {
    this.midiSounds.playChordNow(3, [60], 2.5);
  }

  render() {
    return (
      <>
        <p>
          <button onClick={this.playTestInstrument.bind(this)}>Play</button>
        </p>
        <MIDISounds
          ref={ref => (this.midiSounds = ref)}
          appElementName="root"
          instruments={[3]}
        />
      </>
    );
  }
}
