import React, { Component } from "react";
import { getVexNote } from "../helpers";

import { VFComponent } from "./VFComponent";
/**
 * wraps VFComponent.
 * this is where we will be doing api calls and other
 * things to be able to plug in directly to vexflow component
 */
export default class VFComponentWrapper extends Component {
  state = {
    notes: []
  };
  /**
   * this is where we will make api call to get notes
   */
  addNotes = async () => {
    let noteList = [
      ["c/4"],
      ["d/4"],
      ["b/4"],
      ["d/4"],
      ["e/5"],
      ["d/4"],
      ["c/4"],
      ["e/5"]
    ];
    let notes = [];
    noteList.forEach(note => notes.push(getVexNote("treble", note, "q")));

    await this.setState({
      notes: notes
    });
  };
  render() {
    return (
      <>
        <VFComponent notes={this.state.notes} />
        <button onClick={this.addNotes}>Add Notes</button>
      </>
    );
  }
}
