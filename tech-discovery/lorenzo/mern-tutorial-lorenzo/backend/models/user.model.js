const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    username: {
      type: String, // will be of type string
      required: true, // is a required field
      unique: true, // must be unique
      trim: true, // whitespace will be trimmed if user accidently enters in some
      minlength: 3 // has to be at least 3 characters long
    }
  },
  {
    timestamps: true // will auto make fields for when it was created/modified
  }
);

const User = mongoose.model("User", userSchema);

module.exports = User;
