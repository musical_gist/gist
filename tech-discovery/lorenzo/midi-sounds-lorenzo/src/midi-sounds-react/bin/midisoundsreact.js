"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = (function() {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
  return function(Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
})();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactModal = require("react-modal");

var _reactModal2 = _interopRequireDefault(_reactModal);

var _webaudiofont = require("webaudiofont");

var _webaudiofont2 = _interopRequireDefault(_webaudiofont);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called"
    );
  }
  return call && (typeof call === "object" || typeof call === "function")
    ? call
    : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError(
      "Super expression must either be null or a function, not " +
        typeof superClass
    );
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass)
    Object.setPrototypeOf
      ? Object.setPrototypeOf(subClass, superClass)
      : (subClass.__proto__ = superClass);
}

var STYLE = {
  MIDISoundsInfo: {
    height: "100%",
    overflow: "auto",
    paddingRight: "3px",
    textAlign: "center"
  },
  MIDISoundsClose: {
    textAlign: "center",
    borderTop: "1px solid silver"
  },
  MIDISoundsClose2: {
    textAlign: "right",
    borderBottom: "1px solid silver"
  },
  MIDISoundsEq2: {
    writingMode: "bt-lr" /* IE */,
    WebkitAppearance: "slider-vertical" /* WebKit */,
    width: "0.5cm",
    height: "4cm",
    padding: "0 5px",
    WebkitTransform: [{ rotate: "270deg" }],
    MozTransform: [{ rotate: "270deg" }],
    transform: [{ rotate: "270deg" }]
  },
  MIDISoundsEq: {},
  MIDISoundsVl: {},
  centerTable: {
    margin: "0px auto"
  },
  tdOn: {
    backgroundColor: "rgb(111,145,124)",
    width: "0.5cm",
    height: "0.5cm"
  },
  tdOff: {
    backgroundColor: "rgb(224,224,224)",
    width: "0.5cm",
    height: "0.5cm"
  },
  eqOn: {
    backgroundColor: "rgb(111,145,124)",
    width: "0.5cm",
    height: "0.2cm",
    fontSize: "30%",
    color: "#ffffff"
  },
  eqOff: {
    backgroundColor: "rgb(224,224,224)",
    width: "0.5cm",
    height: "0.2cm"
  }
};

var MIDISounds = (function(_React$Component) {
  _inherits(MIDISounds, _React$Component);

  function MIDISounds(props) {
    _classCallCheck(this, MIDISounds);

    var _this = _possibleConstructorReturn(
      this,
      (MIDISounds.__proto__ || Object.getPrototypeOf(MIDISounds)).call(
        this,
        props
      )
    );

    console.log("MIDISounds v1.2.48");
    _this.state = {
      showModal: false,
      appElementName: _this.props.appElementName,
      instruments: _this.props.instruments,
      drums: _this.props.drums,
      master: 1.0,
      echo: 0.5,
      q32: 0,
      q64: 0,
      q128: 0,
      q256: 0,
      q512: 0,
      q1k: 0,
      q2k: 0,
      q4k: 0,
      q8k: 0,
      q16k: 0
    };
    if (_this.props.appElementName) {
      _reactModal2.default.setAppElement("#" + _this.props.appElementName);
    }
    _this.handleOpenModal = _this.handleOpenModal.bind(_this);
    _this.handleCloseModal = _this.handleCloseModal.bind(_this);
    _this.midiStatus = "?";
    _this.initAudio();
    return _this;
  }

  _createClass(MIDISounds, [
    {
      key: "render",
      value: function render() {
        var _this2 = this;

        this.refreshCache();
        return null;
      }
    },
    {
      key: "contextTime",
      value: function contextTime() {
        return this.audioContext.currentTime;
      }
    },
    {
      key: "onSetNone",
      value: function onSetNone() {
        this.setBand32(0);
        this.setBand64(0);
        this.setBand128(0);
        this.setBand256(0);
        this.setBand512(0);
        this.setBand1k(0);
        this.setBand2k(0);
        this.setBand4k(0);
        this.setBand8k(0);
        this.setBand16k(0);
      }
    },
    {
      key: "onSetDance",
      value: function onSetDance() {
        this.setBand32(2);
        this.setBand64(2);
        this.setBand128(1);
        this.setBand256(-1);
        this.setBand512(5);
        this.setBand1k(4);
        this.setBand2k(4);
        this.setBand4k(2);
        this.setBand8k(-2);
        this.setBand16k(3);
      }
    },
    {
      key: "onSetPower",
      value: function onSetPower() {
        this.setBand32(2);
        this.setBand64(4);
        this.setBand128(3);
        this.setBand256(-2);
        this.setBand512(-3);
        this.setBand1k(1);
        this.setBand2k(2);
        this.setBand4k(3);
        this.setBand8k(-3);
        this.setBand16k(1);
      }
    },
    {
      key: "onChangeMaster",
      value: function onChangeMaster(e) {
        var n = e.target.value;
        this.setMasterVolume(n);
      }
    },
    {
      key: "onChangeEcho",
      value: function onChangeEcho(e) {
        var n = e.target.value;
        this.setEchoLevel(n);
      }
    },
    {
      key: "onChangeQ32",
      value: function onChangeQ32(e) {
        var n = e.target.value;
        this.setBand32(n);
      }
    },
    {
      key: "onChangeQ64",
      value: function onChangeQ64(e) {
        var n = e.target.value;
        this.setBand64(n);
      }
    },
    {
      key: "onChangeQ128",
      value: function onChangeQ128(e) {
        var n = e.target.value;
        this.setBand128(n);
      }
    },
    {
      key: "onChangeQ256",
      value: function onChangeQ256(e) {
        var n = e.target.value;
        this.setBand256(n);
      }
    },
    {
      key: "onChangeQ512",
      value: function onChangeQ512(e) {
        var n = e.target.value;
        this.setBand512(n);
      }
    },
    {
      key: "onChangeQ1k",
      value: function onChangeQ1k(e) {
        var n = e.target.value;
        this.setBand1k(n);
      }
    },
    {
      key: "onChangeQ2k",
      value: function onChangeQ2k(e) {
        var n = e.target.value;
        this.setBand2k(n);
      }
    },
    {
      key: "onChangeQ4k",
      value: function onChangeQ4k(e) {
        var n = e.target.value;
        this.setBand4k(n);
      }
    },
    {
      key: "onChangeQ8k",
      value: function onChangeQ8k(e) {
        var n = e.target.value;
        this.setBand8k(n);
      }
    },
    {
      key: "onChangeQ16k",
      value: function onChangeQ16k(e) {
        var n = e.target.value;
        this.setBand16k(n);
      }
    },
    {
      key: "refreshCache",
      value: function refreshCache() {
        if (this.state.instruments) {
          for (var i = 0; i < this.state.instruments.length; i++) {
            this.cacheInstrument(this.state.instruments[i]);
          }
        }
        if (this.state.drums) {
          for (var k = 0; k < this.state.drums.length; k++) {
            this.cacheDrum(this.state.drums[k]);
          }
        }
      }
    },
    {
      key: "getProperties",
      value: function getProperties() {
        return {
          master: this.echo.output.gain.value * 1
        };
      }
    },
    {
      key: "showPropertiesDialog",
      value: function showPropertiesDialog() {
        this.handleOpenModal();
      }
    },
    {
      key: "handleOpenModal",
      value: function handleOpenModal() {
        this.setState({ showModal: true });
      }
    },
    {
      key: "handleCloseModal",
      value: function handleCloseModal() {
        this.setState({ showModal: false });
      }
    },
    {
      key: "initAudio",
      value: function initAudio() {
        console.log("initAudio M♩D♩Sounds");
        if (this.player) {
          if (this.audioContext) {
            this.player.cancelQueue(this.audioContext);
          }
        }
        var AudioContextFunc = window.AudioContext || window.webkitAudioContext;
        this.audioContext = new AudioContextFunc();
        this.destination = this.audioContext.destination;
        this.player = new _webaudiofont2.default();
        this.equalizer = this.player.createChannel(this.audioContext);
        this.output = this.audioContext.createGain();
        this.echo = this.player.createReverberator(this.audioContext);
        this.echo.wet.gain.setTargetAtTime(this.state.echo, 0, 0.0001);
        this.echo.output.connect(this.output);
        this.equalizer.output.connect(this.echo.input);
        this.output.connect(this.destination);
        this.volumesInstrument = [];
        this.volumesDrum = [];
        this.midiNotes = [];
      }
    },
    {
      key: "cacheInstrument",
      value: function cacheInstrument(n) {
        var info = this.player.loader.instrumentInfo(n);
        if (window[info.variable]) {
          return;
        }
        this.player.loader.startLoad(
          this.audioContext,
          info.url,
          info.variable
        );
        this.player.loader.waitLoad(function() {
          console.log("cached", n, info.title);
        });
      }
    },
    {
      key: "cacheDrum",
      value: function cacheDrum(n) {
        var info = this.player.loader.drumInfo(n);
        if (window[info.variable]) {
          return;
        }
        this.player.loader.startLoad(
          this.audioContext,
          info.url,
          info.variable
        );
        this.player.loader.waitLoad(function() {
          console.log("cached", n, info.title);
        });
      }
    },
    {
      key: "playDrum",
      value: function playDrum(when, drum) {
        var info = this.player.loader.drumInfo(drum);
        if (window[info.variable]) {
          var pitch = window[info.variable].zones[0].keyRangeLow;
          var volume = this.volumeDrumAdjust(drum);
          this.player.queueWaveTable(
            this.audioContext,
            this.equalizer.input,
            window[info.variable],
            when,
            pitch,
            3,
            volume
          );
        } else {
          this.cacheDrum(drum);
        }
      }
    },
    {
      key: "playDrumsAt",
      value: function playDrumsAt(when, drums) {
        for (var i = 0; i < drums.length; i++) {
          this.playDrum(when, drums[i]);
        }
      }
    },
    {
      key: "volumeInstrumentAdjust",
      value: function volumeInstrumentAdjust(instrument) {
        if (!(this.volumesInstrument[instrument] === undefined)) {
          return this.volumesInstrument[instrument];
        }
        return 1;
      }
    },
    {
      key: "volumeDrumAdjust",
      value: function volumeDrumAdjust(drum) {
        if (!(this.volumesDrum[drum] === undefined)) {
          return this.volumesDrum[drum];
        }
        return 1;
      }
    },
    {
      key: "startPlayLoop",
      value: function startPlayLoop(beats, bpm, density, fromBeat) {
        this.stopPlayLoop();
        this.loopStarted = true;
        var wholeNoteDuration = (4 * 60) / bpm;
        if (fromBeat < beats.length) {
          this.beatIndex = fromBeat;
        } else {
          this.beatIndex = 0;
        }
        this.playBeatAt(this.contextTime(), beats[this.beatIndex], bpm);
        var nextLoopTime = this.contextTime() + density * wholeNoteDuration;
        var me = this;
        this.loopIntervalID = setInterval(function() {
          if (me.contextTime() > nextLoopTime - density * wholeNoteDuration) {
            me.beatIndex++;
            if (me.beatIndex >= beats.length) {
              me.beatIndex = 0;
            }
            me.playBeatAt(nextLoopTime, beats[me.beatIndex], bpm);
            nextLoopTime = nextLoopTime + density * wholeNoteDuration;
          }
        }, 22);
      }
    },
    {
      key: "stopPlayLoop",
      value: function stopPlayLoop() {
        this.loopStarted = false;
        clearInterval(this.loopIntervalID);
        this.cancelQueue();
      }
    },
    {
      key: "cancelQueue",
      value: function cancelQueue() {
        this.player.cancelQueue(this.audioContext);
      }
    },
    {
      key: "playBeatAt",
      value: function playBeatAt(when, beat, bpm) {
        this.playDrumsAt(when, beat[0]);
        var chords = beat[1];
        var N = (4 * 60) / bpm;
        for (var i = 0; i < chords.length; i++) {
          var chord = chords[i];
          var instrument = chord[0];
          var pitches = chord[1];
          var duration = chord[2];
          var kind = 0;
          if (chord.length > 3) {
            kind = chord[3];
          }
          if (kind === 1) {
            this.playStrumDownAt(when, instrument, pitches, duration * N);
          } else {
            if (kind === 2) {
              this.playStrumUpAt(when, instrument, pitches, duration * N);
            } else {
              if (kind === 3) {
                this.playSnapAt(when, instrument, pitches, duration * N);
              } else {
                this.playChordAt(when, instrument, pitches, duration * N);
              }
            }
          }
        }
      }
    },
    {
      key: "playChordAt",
      value: function playChordAt(when, instrument, pitches, duration) {
        var info = this.player.loader.instrumentInfo(instrument);
        if (window[info.variable]) {
          this.player.queueChord(
            this.audioContext,
            this.equalizer.input,
            window[info.variable],
            when,
            pitches,
            duration,
            this.volumeInstrumentAdjust(instrument)
          );
        } else {
          this.cacheInstrument(instrument);
        }
      }
    },
    {
      key: "playStrumUpAt",
      value: function playStrumUpAt(when, instrument, pitches, duration) {
        var info = this.player.loader.instrumentInfo(instrument);
        if (window[info.variable]) {
          this.player.queueStrumUp(
            this.audioContext,
            this.equalizer.input,
            window[info.variable],
            when,
            pitches,
            duration,
            this.volumeInstrumentAdjust(instrument)
          );
        } else {
          this.cacheInstrument(instrument);
        }
      }
    },
    {
      key: "playStrumDownAt",
      value: function playStrumDownAt(when, instrument, pitches, duration) {
        var info = this.player.loader.instrumentInfo(instrument);
        if (window[info.variable]) {
          this.player.queueStrumDown(
            this.audioContext,
            this.equalizer.input,
            window[info.variable],
            when,
            pitches,
            duration,
            this.volumeInstrumentAdjust(instrument)
          );
        } else {
          this.cacheInstrument(instrument);
        }
      }
    },
    {
      key: "playSnapAt",
      value: function playSnapAt(when, instrument, pitches, duration) {
        var info = this.player.loader.instrumentInfo(instrument);
        if (window[info.variable]) {
          this.player.queueSnap(
            this.audioContext,
            this.equalizer.input,
            window[info.variable],
            when,
            pitches,
            duration,
            this.volumeInstrumentAdjust(instrument)
          );
        } else {
          this.cacheInstrument(instrument);
        }
      }
    },
    {
      key: "midNoteOn",
      value: function midNoteOn(pitch, velocity) {
        this.midiNoteOff(pitch);
        if (this.miditone) {
          var envelope = this.player.queueWaveTable(
            this.audioContext,
            this.audioContext.destination,
            this.tone,
            0,
            pitch,
            123456789,
            velocity / 100
          );
          var note = {
            pitch: pitch,
            envelope: envelope
          };
          this.midiNotes.push(note);
        }
      }
    },
    {
      key: "midiNoteOff",
      value: function midiNoteOff(pitch) {
        for (var i = 0; i < this.midiNotes.length; i++) {
          if (this.midiNotes[i].pitch === pitch) {
            if (this.midiNotes[i].envelope) {
              this.midiNotes[i].envelope.cancel();
            }
            this.midiNotes.splice(i, 1);
            return;
          }
        }
      }
    },
    {
      key: "midiOnMIDImessage",
      value: function midiOnMIDImessage(event) {
        var data = event.data;
        //var cmd = data[0] >> 4;
        //var channel = data[0] & 0xf;
        var type = data[0] & 0xf0;
        var pitch = data[1];
        var velocity = data[2];
        switch (type) {
          case 144:
            this.midNoteOn(pitch, velocity);
            //logKeys();
            break;
          case 128:
            this.midiNoteOff(pitch);
            //logKeys();
            break;
          default:
            break;
        }
      }
    },
    {
      key: "midiOnStateChange",
      value: function midiOnStateChange(event) {
        console.log("midiOnStateChange", event);
        //msg.innerHTML = event.port.manufacturer + ' ' + event.port.name + ' ' + event.port.state;
      }
    },
    {
      key: "requestMIDIAccessSuccess",
      value: function requestMIDIAccessSuccess(midi) {
        var inputs = midi.inputs.values();
        for (
          var input = inputs.next();
          input && !input.done;
          input = inputs.next()
        ) {
          console.log("midi input", input);
          input.value.onmidimessage = this.midiOnMIDImessage;
        }
        midi.onstatechange = this.midiOnStateChange;
      }
    },
    {
      key: "requestMIDIAccessFailure",
      value: function requestMIDIAccessFailure(e) {
        console.log("requestMIDIAccessFailure", e);
      }
    },
    {
      key: "startMIDIInput",
      value: function startMIDIInput() {
        if (navigator.requestMIDIAccess) {
          console.log("navigator.requestMIDIAccess ok");
          navigator
            .requestMIDIAccess()
            .then(this.requestMIDIAccessSuccess, this.requestMIDIAccessFailure);
        } else {
          console.log("navigator.requestMIDIAccess undefined");
          //msg.innerHTML = 'navigator.requestMIDIAccess undefined';
        }
      }
    },
    {
      key: "playDrumsNow",
      value: function playDrumsNow(drums) {
        this.playDrumsAt(0, drums);
      }
    },
    {
      key: "playChordNow",
      value: function playChordNow(instrument, pitches, duration) {
        this.playChordAt(0, instrument, pitches, duration);
      }
    },
    {
      key: "playStrumUpNow",
      value: function playStrumUpNow(instrument, pitches, duration) {
        this.playStrumUpAt(0, instrument, pitches, duration);
      }
    },
    {
      key: "playStrumDownNow",
      value: function playStrumDownNow(instrument, pitches, duration) {
        this.playStrumDownAt(0, instrument, pitches, duration);
      }
    },
    {
      key: "playSnapNow",
      value: function playSnapNow(instrument, pitches, duration) {
        this.playSnapAt(0, instrument, pitches, duration);
      }
    },
    {
      key: "setMasterVolume",
      value: function setMasterVolume(volume) {
        this.output.gain.setTargetAtTime(volume, 0, 0.0001);
        this.setState({
          master: volume
        });
      }
    },
    {
      key: "setInstrumentVolume",
      value: function setInstrumentVolume(instrument, volume) {
        this.volumesInstrument[instrument] = volume;
      }
    },
    {
      key: "setDrumVolume",
      value: function setDrumVolume(drum, volume) {
        this.volumesDrum[drum] = volume;
      }
    },
    {
      key: "setEchoLevel",
      value: function setEchoLevel(value) {
        this.echo.wet.gain.setTargetAtTime(value, 0, 0.0001);
        this.setState({
          echo: value
        });
      }
    },
    {
      key: "setBand32",
      value: function setBand32(level) {
        this.equalizer.band32.gain.setTargetAtTime(level, 0, 0.0001);
        this.setState({
          q32: level
        });
      }
    },
    {
      key: "setBand64",
      value: function setBand64(level) {
        this.equalizer.band64.gain.setTargetAtTime(level, 0, 0.0001);
        this.setState({
          q64: level
        });
      }
    },
    {
      key: "setBand128",
      value: function setBand128(level) {
        this.equalizer.band128.gain.setTargetAtTime(level, 0, 0.0001);
        this.setState({
          q128: level
        });
      }
    },
    {
      key: "setBand256",
      value: function setBand256(level) {
        this.equalizer.band256.gain.setTargetAtTime(level, 0, 0.0001);
        this.setState({
          q256: level
        });
      }
    },
    {
      key: "setBand512",
      value: function setBand512(level) {
        this.equalizer.band512.gain.setTargetAtTime(level, 0, 0.0001);
        this.setState({
          q512: level
        });
      }
    },
    {
      key: "setBand1k",
      value: function setBand1k(level) {
        this.equalizer.band1k.gain.setTargetAtTime(level, 0, 0.0001);
        this.setState({
          q1k: level
        });
      }
    },
    {
      key: "setBand2k",
      value: function setBand2k(level) {
        this.equalizer.band2k.gain.setTargetAtTime(level, 0, 0.0001);
        this.setState({
          q2k: level
        });
      }
    },
    {
      key: "setBand4k",
      value: function setBand4k(level) {
        this.equalizer.band4k.gain.setTargetAtTime(level, 0, 0.0001);
        this.setState({
          q4k: level
        });
      }
    },
    {
      key: "setBand8k",
      value: function setBand8k(level) {
        this.equalizer.band8k.gain.setTargetAtTime(level, 0, 0.0001);
        this.setState({
          q8k: level
        });
      }
    },
    {
      key: "setBand16k",
      value: function setBand16k(level) {
        this.equalizer.band16k.gain.setTargetAtTime(level, 0, 0.0001);
        this.setState({
          q16k: level
        });
      }
    },
    {
      key: "setKeyboardInstrument",
      value: function setKeyboardInstrument(n) {
        var info = this.player.loader.instrumentInfo(n);
        if (window[info.variable]) {
          this.miditone = window[info.variable];
          return;
        }
        this.player.loader.startLoad(
          this.audioContext,
          info.url,
          info.variable
        );
        this.player.loader.waitLoad(function() {
          console.log("cached", n, info.title);
          this.miditone = window[info.variable];
        });
      }
    }
  ]);

  return MIDISounds;
})(_react2.default.Component);

exports.default = MIDISounds;
