import React, { Component } from "react";
import { Line } from "../entities/entities";

export interface IGLineProps {
  line: Line;
  width: number;
  color: string;
  topLine?: boolean;
  timeSignatureBeat?: number;
  timeSignatureNote?: number;
}

export class GLine extends Component<IGLineProps> {
  render() {
    const { width, color } = this.props;

    if (color === "black") {
      if (this.props.topLine) {
        return (
          <div
            style={{
              width: width,
              height: "1px",
              borderTop: "1.5px solid"
            }}
          />
        );
      } else {
        return (
          <div
            style={{
              width: width,
              height: "6px",
              borderBottom: "1.5px solid"
            }}
          />
        );
      }
    } else {
      return <div style={{ width: this.props.width, height: "6px" }} />;
    }
  }
}
