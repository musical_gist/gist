import React from "react";
import "./App.css";
import { Menu, Popover, Button } from "evergreen-ui";

const App: React.FC = () => {
  return (
    <>
      <Popover
        content={
          <Menu>
            <Menu.Group>
              <Menu.Item icon="people">Share...</Menu.Item>
              <Menu.Item icon="circle-arrow-right">Move...</Menu.Item>
              <Menu.Item icon="edit" secondaryText="⌘R">
                Rename...
              </Menu.Item>
            </Menu.Group>
            <Menu.Divider />
            <Menu.Group>
              <Menu.Item icon="trash" intent="danger">
                Delete...
              </Menu.Item>
            </Menu.Group>
          </Menu>
        }
      >
        <Button marginRight={16}>With Icons</Button>
      </Popover>
    </>
  );
};

export default App;
